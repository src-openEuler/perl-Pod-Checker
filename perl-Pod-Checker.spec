%global perl_package_name Pod-Checker

Name:           perl-%{perl_package_name}
Epoch:          4
Version:        1.77
Release:        2
Summary:	Check pod documents for syntax errors
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://search.cpan.org/dist/%{perl_package_name}
Source0:        https://www.cpan.org/authors/id/M/MA/MAREKR/%{perl_package_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:	gcc findutils make
BuildRequires:  perl-interpreter perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) perl(Config) perl(Cwd)
BuildRequires:  perl(File::Basename) perl(File::Spec) perl(base)
BuildRequires:  perl(Carp) perl(Exporter) perl(Pod::Simple) >= 3.28
BuildRequires:  perl(Pod::Simple::Methody) perl(strict) perl(warnings)
BuildRequires:  perl(FileHandle) perl(Test::More) perl(vars)
Requires:       perl(Pod::Simple) >= 3.28

%description
podchecker will perform syntax checking of Perl5 POD format documentation.

%package_help

%prep
%autosetup -n %{perl_package_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 UNINST=0
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc CHANGES README
%{_bindir}/*
%{perl_vendorlib}/*

%files help
%{_mandir}/man{1/*,3/*}

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 4:1.77-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Nov 18 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 4:1.77-1
- upgrade version to 1.77
- CPAN#151316: Fix INSTALLDIRS option on newer perls
- use sane tar options to create distro
- CPAN#149267: "OK" message should go to STDOUT, not STDERR
- CPAN#150660: podchecker -quiet does not print "OK" message

* Mon Aug 7 2023 zhangyao <zhangyao108@huawei.com> - 4:1.75-1
- upgrade version to 1.75

* Mon Oct 31 2022 hongjinghao <hongjinghao@huawei.com> - 4:1.74-2
- use perl_package_name marco

* Tue Jan 26 2021 liudabo <liudabo1@huawei.com> - 4:1.74-1
- upgrade version to 1.74

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 4:1.73-398
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 4:1.73-397
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise requires

* Wed Sep 04 2019 openEuler Buildtam <buildteam@openeuler.org> - 4:1.73-396
- Package Init
